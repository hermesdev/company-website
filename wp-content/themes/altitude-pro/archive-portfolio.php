<?php
/**
 * This file adds the custom portfolio post type archive template to the Altitude Pro Theme.
 *
 */

//* Add portfolio body class
add_filter( 'body_class', 'altitude_add_portfolio_body_class' );
function altitude_add_portfolio_body_class( $classes ) {
  $classes[] = 'altitude-pro-portfolio';
  return $classes;
}

// //* Load Isotope
// wp_enqueue_script( 'isotope', CHILD_URL . '/js/jquery.isotope.min.js', array( 'jquery' ), '1.5.26', true );
// wp_enqueue_script( 'isotope_init', CHILD_URL . '/js/hermesdev_isotope_init.js', array( 'isotope' ), '1.0.0', true );

// //* Load JS that sets the height of each grid item to that of the image in it
// wp_enqueue_script( 'portfolio-archive', CHILD_URL .'/js/hermesdev_portfolio-archive.js' , array( 'jquery' ), '1.0.0', true );


//* Force full width content layout
add_filter( 'genesis_pre_get_option_site_layout', '__genesis_return_full_width_content' );

//* Display Portfolio Categories Filter
add_action( 'genesis_before_loop', 'sk_isotope_filter' );
function sk_isotope_filter() {

  if ( is_post_type_archive( 'portfolio' ) )

    $terms = get_terms( 'portfolio_category' );
    $count = count( $terms ); $i=0;
    if ( $count > 0 ) { ?>
      <ul id="portfolio-cats" class="filter clearfix">
        <li><a href="#" class="active" data-filter="*"><span><?php _e('All', 'genesis'); ?></span></a></li>
        <?php foreach ( $terms as $term ) : ?>
          <li><a href="#" data-filter=".<?php echo $term->slug; ?>"><span><?php echo $term->name; ?></span></a></li>
        <?php endforeach; ?>
      </ul><!-- /portfolio-cats -->
    <?php }

}

//* Display 'back to portfolio' link and Title on Portfolio Category archive pages
add_action( 'genesis_before_loop', 'sk_taxonomy_page_additions' );
function sk_taxonomy_page_additions() {

  if ( is_tax( 'portfolio_category' ) ) {

    echo '<a href="' . get_bloginfo( 'url' ) . '/portfolio/">&laquo; Back to Full Portfolio</a>';
    global $wp_query;

    $term = $wp_query->get_queried_object();
    echo '<h2 class="taxonomy-title">' . $term->name . '</h2>';

  }
}

//* Wrap Portfolio items in a custom div - opening
add_action('genesis_before_loop', 'portfolio_content_opening_div' );
function portfolio_content_opening_div() {

  echo '<div class="portfolio-content">';

}

//* Remove the breadcrumb navigation
remove_action( 'genesis_before_loop', 'genesis_do_breadcrumbs' );

//* Remove post info function
remove_action( 'genesis_entry_header', 'genesis_post_info', 5 );

//* Force Excerpts
add_filter( 'genesis_pre_get_option_content_archive', 'sk_show_excerpts' );
function sk_show_excerpts() {
  return 'excerpts';
}

//* Modify the length of post excerpts
add_filter( 'excerpt_length', 'sp_excerpt_length' );
function sp_excerpt_length( $length ) {
  return 10; // pull first 10 words
}

//* Modify the Excerpt read more link
add_filter( 'excerpt_more', 'new_excerpt_more' );
function new_excerpt_more( $more ) {
    return '... <a href="' . get_permalink() . '">More</a>';
}

//* Remove the post image
remove_action( 'genesis_entry_content', 'genesis_do_post_image', 8 );

//* Remove post meta function
remove_action( 'genesis_entry_footer', 'genesis_post_meta' );

//* Do not show Featured image if set in Theme Settings > Content Archives
add_filter( 'genesis_pre_get_option_content_archive_thumbnail', '__return_false' );

//* Add featured image above .entry-header
add_action( 'genesis_entry_header', 'altitude_portfolio_grid', 4 );
function altitude_portfolio_grid() {

  if ( $image = genesis_get_image( 'format=url&size=portfolio' ) ) {
    printf( '<div class="portfolio-featured-image"><a href="%s" rel="bookmark"><img src="%s" alt="%s" /></a></div>', get_permalink(), $image, the_title_attribute( 'echo=0' ) );

  }

}

//* Wrap .entry-header and .entry-content in a custom div - opening
add_action( 'genesis_entry_header', 'sk_opening_div', 4 );
function sk_opening_div() {
  echo '<div class="entry-content-wrap">';
}

//* Wrap .entry-header and .entry-content in a custom div - closing
add_action( 'genesis_entry_footer', 'sk_closing_div' );
function sk_closing_div() {
  echo '</div>';
}

//* add category names in post class
add_filter('post_class', 'portfolio_category_class');
function portfolio_category_class($classes) {

  $terms = get_the_terms( get_the_ID(), 'portfolio_category' );
  if( $terms ) foreach ( $terms as $term )
    $classes[] = $term->slug;

  return $classes;

}

//* Wrap Portfolio items in a custom div - closing
add_action('genesis_after_loop', 'portfolio_content_closing_div' );
function portfolio_content_closing_div() {

  echo "</div>";

}

genesis();