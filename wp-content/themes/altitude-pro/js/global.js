jQuery(function( $ ){

	if( $( document ).scrollTop() > 0 ){
		$( '.site-header' ).addClass( 'dark' );			
	}

	// Add opacity class to site header
	$( document ).on('scroll', function(){

		if ( $( document ).scrollTop() > 0 ){
			$( '.site-header' ).addClass( 'dark' );			

		} else {
			$( '.site-header' ).removeClass( 'dark' );			
		}

	});

	$( '.nav-primary .genesis-nav-menu, .nav-secondary .genesis-nav-menu' ).addClass( 'responsive-menu' ).before('<div class="responsive-menu-icon"></div>');

	$( '.responsive-menu-icon' ).click(function(){
		$(this).next( '.nav-primary .genesis-nav-menu,  .nav-secondary .genesis-nav-menu' ).slideToggle();
	});

	$( window ).resize(function(){
		if ( window.innerWidth > 800 ) {
			$( '.nav-primary .genesis-nav-menu,  .nav-secondary .genesis-nav-menu, nav .sub-menu' ).removeAttr( 'style' );
			$( '.responsive-menu > .menu-item' ).removeClass( 'menu-open' );
		}
	});

	$( '.responsive-menu > .menu-item' ).click(function(event){
		if ( event.target !== this )
		return;
			$(this).find( '.sub-menu:first' ).slideToggle(function() {
			$(this).parent().toggleClass( 'menu-open' );
		});
	});

/*
 * Dynamic current-menu-item class for the single page layout
 *
 */
var sections = [];
for(var i = 0; i < 5; i++) {
	sections.push($('[id=front-page-'+ (i + 1) +']'))
}

// var $sections = $('class^=front-page-');
	console.log(sections);

	$(document).on('scroll', function() {
		
		var scroll        = $(window).scrollTop(),
        defaultOffset = 100,
        lastOffset    = null;

	  $.each(sections, function(index, value) {
	  	
	    var verticalPosition = $(this).offset().top;

	    if(index + 1 < sections.length) {
	      $nextSection     = $(sections[index + 1]);
	    } else {
	    	var $nextSection = null;
	    }
	       
	    if(lastOffset === null) {
	    	var verticalStart = verticalPosition;
	    } else {
	    	var verticalStart = verticalPosition - lastOffset;
	    }

	    if($nextSection === null) {
	    	var verticalEnd = verticalPosition + Math.abs($(document).height() - verticalPosition);
	    } else {
	    	lastOffset      = Math.abs(verticalPosition - $nextSection.offset().top) / 2
				var verticalEnd = verticalPosition + lastOffset;
	    }

			// get the menu item related the current section
			var $menuItem = $('[href^="#' + $(this)[0].id + '"]').parent();

			// remove all the current-menu-item classes
	    $menuItem.removeClass('current-menu-item');

	    if(scroll >= verticalStart && scroll <= verticalEnd) {
	      $menuItem.addClass('current-menu-item');
	    }

		});
	})

});