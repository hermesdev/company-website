jQuery(function( $ ){
 
  function setHeights() {
    $('.altitude-pro-portfolio .entry').each(function() {
      var $imgh = $('.altitude-pro-portfolio .entry img').height();
      $(this).css('min-height', $imgh);
    });
  }
  $(window).on('resize load', setHeights);
 
});