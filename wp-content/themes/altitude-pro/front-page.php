<?php
/**
 * This file adds the Home Page to the Altitude Pro Theme.
 *
 * @author StudioPress
 * @package Altitude
 * @subpackage Customizations
 */

add_action( 'genesis_meta', 'altitude_front_page_genesis_meta' );
/**
 * Add widget support for homepage. If no widgets active, display the default loop.
 *
 */
function altitude_front_page_genesis_meta() {

	// if ( is_active_sidebar( 'front-page-1' ) || is_active_sidebar( 'front-page-2' ) || is_active_sidebar( 'front-page-3' ) || is_active_sidebar( 'front-page-4' ) || is_active_sidebar( 'front-page-5' ) || is_active_sidebar( 'front-page-6' ) || is_active_sidebar( 'front-page-7' ) ) {
	if ( is_active_sidebar( 'front-page-1' ) || is_active_sidebar( 'front-page-2' ) || is_active_sidebar( 'front-page-3' ) || is_active_sidebar( 'front-page-4' ) || is_active_sidebar( 'front-page-6' ) || is_active_sidebar( 'front-page-7' ) ) {

		//* Enqueue scripts
		add_action( 'wp_enqueue_scripts', 'altitude_enqueue_altitude_script' );
		function altitude_enqueue_altitude_script() {

			wp_enqueue_script( 'altitude-script', get_bloginfo( 'stylesheet_directory' ) . '/js/home.js', array( 'jquery' ), '1.0.0' );
			wp_enqueue_script( 'localScroll', get_stylesheet_directory_uri() . '/js/jquery.localScroll.min.js', array( 'scrollTo' ), '1.2.8b', true );
			wp_enqueue_script( 'scrollTo', get_stylesheet_directory_uri() . '/js/jquery.scrollTo.min.js', array( 'jquery' ), '1.4.5-beta', true );

			//* Load Isotope
			wp_enqueue_script( 'isotope', get_stylesheet_directory_uri() . '/js/jquery.isotope.min.js', array( 'jquery' ), '1.5.26', true );
			wp_enqueue_script( 'isotope_init', get_stylesheet_directory_uri() . '/js/hermesdev_isotope_init.js', array( 'isotope' ), '1.0.0', true );

		}

		//* Add front-page body class
		add_filter( 'body_class', 'altitude_body_class' );
		function altitude_body_class( $classes ) {

   			$classes[] = 'front-page altitude-pro-portfolio';
  			return $classes;

		}

		//* Force full width content layout
		add_filter( 'genesis_pre_get_option_site_layout', '__genesis_return_full_width_content' );

		//* Remove breadcrumbs
		remove_action( 'genesis_before_loop', 'genesis_do_breadcrumbs' );

		//* Remove the default Genesis loop
		remove_action( 'genesis_loop', 'genesis_do_loop' );

		//* Add homepage widgets
		add_action( 'genesis_loop', 'altitude_front_page_widgets' );

		//* Add featured-section body class
		if ( is_active_sidebar( 'front-page-1' ) ) {

			//* Add image-section-start body class
			add_filter( 'body_class', 'altitude_featured_body_class' );
			function altitude_featured_body_class( $classes ) {

				$classes[] = 'featured-section';				
				return $classes;

			}

		}

	}

}

//* Add markup for front page widgets
function altitude_front_page_widgets() {

	genesis_widget_area( 'front-page-1', array(
		'before' => '<div id="front-page-1" class="front-page-1"><div class="image-section"><div class="flexible-widgets widget-area' . altitude_widget_area_class( 'front-page-1' ) . '"><div class="wrap">',
		'after'  => '</div></div></div></div>',
	) );

	genesis_widget_area( 'front-page-2', array(
		'before' => '<div id="front-page-2" class="front-page-2"><div class="solid-section"><div class="flexible-widgets widget-area' . altitude_widget_area_class( 'front-page-2' ) . '"><div class="wrap">',
		'after'  => '</div></div></div></div>',
	) );

	genesis_widget_area( 'front-page-3', array(
		'before' => '<div id="front-page-3" class="front-page-3"><div class="image-section"><div class="flexible-widgets widget-area' . altitude_widget_area_class( 'front-page-3' ) . '"><div class="wrap">',
		'after'  => '</div></div></div></div>',
	) );

	genesis_widget_area( 'front-page-4', array(
		'before' => '<div id="front-page-4" class="front-page-4"><div class="solid-section"><div class="flexible-widgets widget-area' . altitude_widget_area_class( 'front-page-4' ) . '"><div class="wrap">',
		'after'  => '</div><div id="non-profit">
		<h3>Non-profit or a worthy cause? We\'ll build your site at discounted rates.</h3>
		<h3><a href="http://hermesdevelopment.com/contact">Let us know.</a></h3>
		</div></div>',
	) );

	// genesis_widget_area( 'front-page-5', array(
	// 	'before' => '<div id="front-page-5" class="front-page-5"><div class="image-section"><div class="flexible-widgets widget-area' . altitude_widget_area_class( 'front-page-5' ) . '"><div class="wrap">',
	// 	'after'  => '</div></div></div></div>',
	// ) );
	sk_filterable_portfolio();

	genesis_widget_area( 'front-page-6', array(
		'before' => '<div id="front-page-6" class="front-page-6"><div class="solid-section"><div class="flexible-widgets widget-area' . altitude_widget_area_class( 'front-page-6' ) . '"><div class="wrap">',
		'after'  => '</div></div></div></div>',
	) );

	genesis_widget_area( 'front-page-7', array(
		'before' => '<div id="front-page-7" class="front-page-7"><div class="image-section"><div class="flexible-widgets widget-area' . altitude_widget_area_class( 'front-page-7' ) . '"><div class="wrap">',
		'after'  => '</div></div></div></div>',
	) );

}

function custom_read_more() {
	return '... <a href="' . get_permalink() . '">More</a>';
}

function excerpt( $limit ) {
	return wp_trim_words( get_the_excerpt(), $limit, custom_read_more() );
}

function sk_filterable_portfolio() {
	// Display Portfolio Categories
	$terms = get_terms( 'portfolio_category' );
	$count = count( $terms ); $i=0;
	if ( $count > 0 ) { ?>
		<div id="front-page-5" class="front-page-5">
			<div class="portfolio-title">
				<h4>Our Work</h4>
				<h2>Check out what<br> we've built.</h2>
				<br>
			</div>
			<div class="wrap">
				<ul id="portfolio-cats" class="filter clearfix">
					<li><a href="#" class="active" data-filter="*"><span><?php _e('All', 'genesis'); ?></span></a></li>
					<?php foreach ( $terms as $term ) : ?>
						<li><a href="#" data-filter=".<?php echo $term->slug; ?>"><span><?php echo $term->name; ?></span></a></li>
					<?php endforeach; ?>
				</ul><!-- /portfolio-cats -->
	<?php }

	// Query
	$sk_port_query = new WP_Query(
		array(
			'post_type' => 'portfolio',
			'showposts' => '6'
		)
	);

	// Loop
	if( $sk_port_query->posts ) { ?>
		<div class="portfolio-wrap">
			<div class="portfolio-content">
				<?php while ( $sk_port_query->have_posts() ) : $sk_port_query->the_post();

					$terms = get_the_terms( get_the_ID(), 'portfolio_category' );

					$video_url = get_post_meta( get_the_ID(), 'video_url', true );

					if ( has_post_thumbnail( $post->ID ) ) { ?>
						<article class="entry portfolio-item <?php if( $terms ) foreach ( $terms as $term ) { echo $term->slug . ' '; }; ?>">
							<div class="portfolio-featured-image">
								<a href="<?php the_permalink() ?>" rel="bookmark" title="<?php the_title_attribute(); ?>">
								<?php echo genesis_get_image( array( size => 'portfolio' ) ); ?></a>
							</div>
							<div class="entry-content-wrap">
								<header class="entry-header">
									<h1 itemprop="headline" class="entry-title"><a rel="bookmark" href="<?php the_permalink() ?>"><?php the_title(); ?></a></h1> 
								</header>
								<div itemprop="text" class="entry-content">
									<p><?php echo excerpt( '10' ); ?></p>
								</div>
								<?php if ( $video_url ) { ?>
									<div class="video-button"><a href="<?php echo $video_url; ?>" class="button foobox">Click to watch the video</a></div>
								<?php } ?>
							</div>
						</article>
					<?php } ?>
				<?php endwhile; ?>
			</div><!-- /portfolio-content -->
		</div><!-- /portfolio-wrap -->
		<!-- <div class="portfolio-archive-link"><a href="/portfolio/" class="button">View our full Portfolio &raquo;</a></div> -->
		<?php } ?>
		<?php wp_reset_postdata(); ?>
	</div></div> <!-- /wrap, /front-page-5 -->
 
<?php }

genesis();
